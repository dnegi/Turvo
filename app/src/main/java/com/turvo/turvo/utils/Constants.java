package com.turvo.turvo.utils;

/**
 * Created by lp-191 on 05/11/17.
 */

public interface Constants {
    public int DATE_LOWER_LIMIT =8;
    public int DATE_UPPER_LIMIT =10;
    public int PRICE_LOWER_LIMIT =0;
    public int PRICE_UPPER_LIMIT =999;

    public String STOCK_DATA = "STOCK_DATA";
}
