package com.turvo.turvo.utils;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by lp-191 on 05/11/17.
 */

public class Helper {
    public static int getIndex(int[] data, int value, boolean isBuy, ArrayList<Integer> exemptedIndex){
        if(isBuy){
            for (int i = 0; i < data.length; i++) {
                if(data[i] == value ){
                    if(exemptedIndex.size() >0) {
                        for (int j = 0; j < exemptedIndex.size(); j++) {
                            if (exemptedIndex.get(j) == i) {
                                break;
                            } else if (j == exemptedIndex.size() - 1) {
                                return i;
                            }
                        }
                    }else{
                        return i;
                    }
                }
            }
        } else {
            for (int i = (data.length -1); i >= 0 ; i--) {
                if( data[i] == value){
                    if(exemptedIndex.size() >0) {
                        for (int j = 0; j < exemptedIndex.size(); j++) {
                            if (exemptedIndex.get(j) == i) {
                                break;
                            } else if (j == exemptedIndex.size() - 1) {
                                return i;
                            }
                        }
                    } else {
                        return i;
                    }
                }
            }
        }
        return -1;
    }
}
