package com.turvo.turvo.data;

import java.util.ArrayList;

/**
 * Created by lp-191 on 05/11/17.
 */

public interface Analysis {
    public ArrayList<Trade> findBestTrades(int pairCount);
    public int findMaxProfit();
}
