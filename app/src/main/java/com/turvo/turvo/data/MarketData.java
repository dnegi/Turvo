package com.turvo.turvo.data;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by lp-191 on 05/11/17.
 */

public class MarketData {
    public ArrayList<StockData> getStocks() {
        return stocks;
    }

    ArrayList<StockData> stocks = new ArrayList<>();
    private int dateRange;
    private int lowerLimit;
    private int upperLimit;


    private MarketData(int dateRange, int lowerLimit, int upperLimit ){
        this.dateRange = dateRange;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;

        stocks.add( new StockData("Mahindra", "MAH", dateRange, lowerLimit, upperLimit));
        stocks.add( new StockData("Apple", "APPL", dateRange, lowerLimit, upperLimit));
        stocks.add( new StockData("Amazon", "AMZ", dateRange, lowerLimit, upperLimit));
        stocks.add( new StockData("Google", "GOOG", dateRange, lowerLimit, upperLimit));
        stocks.add( new StockData("Punjab National Bank", "PNB", dateRange, lowerLimit, upperLimit));
        stocks.add( new StockData("State Bank of India", "SBI", dateRange, lowerLimit, upperLimit));
    }

    public static class Builder{
        private int dateRange;
        private int lowerLimit;
        private int upperLimit;

        public Builder(){}

        public Builder setDateRange(int dateRange) {
            this.dateRange = dateRange;
            return this;
        }

        public Builder setLowerLimit(int lowerLimit) {
            this.lowerLimit = lowerLimit;
            return this;
        }

        public Builder setUpperLimit(int upperLimit) {
            this.upperLimit = upperLimit;
            return this;
        }

        public MarketData build() {
            return new MarketData(dateRange, lowerLimit, upperLimit);
        }
    }





}
