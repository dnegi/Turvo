package com.turvo.turvo.data;

import java.util.Random;

/**
 * Created by lp-191 on 05/11/17.
 */

abstract class PriceGenerator {

    public int[] generatePrices(int dateRange, int lowerLimit, int upperLimit) {
        int a[] = new int[dateRange];
        Random rand = new Random();
        for (int i = 0; i < dateRange ; i++) {
            a[i] = lowerLimit + rand.nextInt(upperLimit - lowerLimit +1);
        }
        return a;
    }
}
