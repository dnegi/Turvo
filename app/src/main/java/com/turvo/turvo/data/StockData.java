package com.turvo.turvo.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.turvo.turvo.utils.Constants;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by lp-191 on 05/11/17.
 */

public class StockData extends PriceGenerator implements Analysis, Parcelable {
    String stockName;
    String stockTicker;
    int[] stockValue;
    int dateRange;
    int lowerLimit;
    int upperLimit;

    public StockData(String stockName, String stockTicker, int dateRange, int lowerLimit, int upperLimit){
        this.stockName = stockName;
        this.stockTicker = stockTicker;

        if(dateRange == 0) {
            Random rand = new Random();
            this.dateRange = Constants.DATE_UPPER_LIMIT +
                    rand.nextInt(Constants.DATE_UPPER_LIMIT - Constants.DATE_LOWER_LIMIT +1);
        } else{
            this.dateRange = dateRange;
        }

        if(lowerLimit == 0){
            this.lowerLimit = Constants.PRICE_LOWER_LIMIT;
        } else {
            this.lowerLimit = lowerLimit;
        }

        if(upperLimit == 0){
            this.upperLimit = Constants.PRICE_UPPER_LIMIT;
        } else {
            this.upperLimit = upperLimit;
        }
        updateStockPrice();
    }

    public void updateStockPrice(){
        this.stockValue = generatePrices(dateRange, lowerLimit, upperLimit);
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public int[] getStockValue() {
        return stockValue;
    }

    public void setStockValue(int[] stockValue) {
        this.stockValue = stockValue;
    }

    public int getLatestStockPrice(){
        return stockValue[stockValue.length -1];
    }

    public int latestChange(){
        return stockValue[stockValue.length -1] - stockValue[stockValue.length -2];
    }

    @Override
    public ArrayList<Trade> findBestTrades(int pairCount) {
        ArrayList<Trade> result = new ArrayList<>();
        ArrayList<Integer> priceData = new ArrayList<>();
        for (int i = 0; i < this.stockValue.length; i++) {
            priceData.add(stockValue[i]);
        }
        for(int i = 0; i < pairCount; i++){
            int buyIndex = 0;
            int sellIndex = priceData.get(1) > priceData.get(0)? 1:0;
            int currentBuyIndex = 0;
            int maxGain = priceData.get(1) - priceData.get(0);
            for (int j = 1; j < priceData.size(); j++) {
                if (priceData.get(j) - priceData.get(currentBuyIndex) > maxGain) {
                    maxGain = priceData.get(j) - priceData.get(currentBuyIndex);
                    buyIndex = currentBuyIndex;
                    sellIndex = j;
                }
                if (priceData.get(j) < priceData.get(buyIndex))
                    currentBuyIndex = j;
            }
            if(sellIndex != 0){
                result.add(new Trade(priceData.get(buyIndex), priceData.get(sellIndex)));
            }
            priceData.remove(sellIndex);
            priceData.remove(buyIndex);
        }

        return result;

    }

    @Override
    public int findMaxProfit() {
    int[] profit = new int[stockValue.length];
    profit[0] = 0;
    profit[1] = stockValue[1]- stockValue[0] > 0 ? stockValue[1]-stockValue[0]: 0;

    for (int i = 2; i < stockValue.length; i++) {
        profit[i] = profit[i-2] + (stockValue[i] - stockValue[i-1]) > profit[i-1] ?
                profit[i-2] + (stockValue[i] - stockValue[i-1]) : profit[i-1];
    }
     return profit[stockValue.length -1];
    }


    public static final Creator<StockData> CREATOR = new Creator<StockData>() {
        @Override
        public StockData createFromParcel(Parcel in) {
            return new StockData(in);
        }

        @Override
        public StockData[] newArray(int size) {
            return new StockData[size];
        }
    };

    public StockData(Parcel in){
        this.stockName = in.readString();
        this.stockTicker = in.readString();
        this.stockValue =  in.createIntArray();
        this.dateRange =  in.readInt();
        this.lowerLimit =  in.readInt();
        this.upperLimit =  in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(stockName);
        parcel.writeString(stockTicker);
        parcel.writeIntArray(stockValue);
        parcel.writeInt(dateRange);
        parcel.writeInt(lowerLimit);
        parcel.writeInt(upperLimit);
    }
}
