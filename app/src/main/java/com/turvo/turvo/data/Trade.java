package com.turvo.turvo.data;

/**
 * Created by lp-191 on 05/11/17.
 */

public class Trade {
    public int getBuyPrice() {
        return buyPrice;
    }

    public int getSellPrice() {
        return sellPrice;
    }

    private int buyPrice;
    private int sellPrice;

    public Trade(int buyPrice, int sellPrice){
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
    }
}
