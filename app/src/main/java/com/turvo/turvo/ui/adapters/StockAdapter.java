package com.turvo.turvo.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

//import com.jakewharton.rxbinding.view.RxView;
import com.turvo.turvo.R;
import com.turvo.turvo.utils.Constants;
import com.turvo.turvo.data.StockData;
import com.turvo.turvo.ui.activities.StockGraphActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lp-191 on 05/11/17.
 */

public class StockAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private final LayoutInflater mLayoutInflater;
    private final Context mContext;
    private List<StockData> mStocks;
    private OnClickHandler onClickHandler = new OnClickHandler();

    public StockAdapter(Context context) {
//        this.mStocks = stocks;
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    public void setData(ArrayList<StockData> stocks) {
        this.mStocks = stocks;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_stock, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final StockData stock = mStocks.get(position);
        ItemHolder itemHolder = (ItemHolder) holder;

        itemHolder.stockName.setText(stock.getStockName());
        itemHolder.stockTicker.setText(stock.getStockTicker());
        itemHolder.stockPrice.setText("\u20B9 " +String.valueOf(stock.getLatestStockPrice()));

        if(stock.latestChange() > 0){
            itemHolder.stockPrice.setCompoundDrawablesWithIntrinsicBounds(0,0,android.R.drawable.arrow_up_float,0);
            itemHolder.stockPrice.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
        } else if( stock.latestChange() < 0){
            itemHolder.stockPrice.setCompoundDrawablesWithIntrinsicBounds(0,0,android.R.drawable.arrow_down_float,0);
            itemHolder.stockPrice.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_light));
        } else{
            itemHolder.stockPrice.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
            itemHolder.stockPrice.setTextColor(ContextCompat.getColor(mContext, R.color.text_color));

        }

        itemHolder.showStockData.setTag(position);
        itemHolder.showStockData.setOnClickListener(onClickHandler);

//        RxView.clicks(itemHolder.showStockData)
//                .subscribe(view -> {
//                    Intent intent = new Intent(mContext, StockGraphActivity.class);
//                    intent.putExtra(Constants.STOCK_DATA, mStocks.get(position));
//                    mContext.startActivity(intent);
//                });


    }

    @Override
    public int getItemCount() {
        return mStocks.size();
    }

    protected static class ItemHolder extends RecyclerView.ViewHolder {
        TextView stockName;
        TextView stockTicker;
        TextView stockPrice;
        ImageView showStockData;

        public ItemHolder(View itemView) {
            super(itemView);
            stockName = (TextView)itemView.findViewById(R.id.stock_name);
            stockTicker = (TextView) itemView.findViewById(R.id.stock_ticker);
            stockPrice = (TextView)itemView.findViewById(R.id.stock_value);
            showStockData = (ImageView) itemView.findViewById(R.id.iv_show_stock);

        }
    }

    private class OnClickHandler implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();

            switch(viewId) {
                case R.id.iv_show_stock:
                    showStockData(view);
                    break;
            }
        }
    }


    private void showStockData(View view) {
        Object objectTag = view.getTag();
        Integer position;
        if (objectTag != null) {
            position = (Integer) objectTag;
            Intent intent = new Intent(mContext, StockGraphActivity.class);
            intent.putExtra(Constants.STOCK_DATA, mStocks.get(position));
            mContext.startActivity(intent);
        }
    }
}

