package com.turvo.turvo.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.turvo.turvo.R;
import com.turvo.turvo.data.MarketData;
import com.turvo.turvo.data.StockData;
import com.turvo.turvo.ui.adapters.StockAdapter;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {
    private MarketData mData;
    private RecyclerView mRecyclerView;
    private StockAdapter mAdapter;
    private Disposable subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdapter = new StockAdapter(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_stocks);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
        setInputData();
        createObservable();
    }

    private void createObservable() {
        Observable<ArrayList<StockData>> stocksObservable
            = Observable.just(mData.getStocks());
        subscription = stocksObservable
                .subscribe(stocks -> updateAdapter(stocks));
    }

    private void updateAdapter(ArrayList<StockData> stocks) {
        mAdapter.setData(stocks);
    }

    private void setInputData() {
        mData = new MarketData.Builder()
                .setDateRange(9)
                .build();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }
    }
}
