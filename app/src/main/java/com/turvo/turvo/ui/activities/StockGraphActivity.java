package com.turvo.turvo.ui.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.turvo.turvo.utils.Constants;
import com.turvo.turvo.R;
import com.turvo.turvo.utils.DayAxisValueFormatter;
import com.turvo.turvo.data.StockData;
import com.turvo.turvo.data.Trade;
import com.turvo.turvo.ui.custom.XYMarkerView;
import com.turvo.turvo.utils.Helper;

import java.util.ArrayList;

/**
 * Created by lp-191 on 05/11/17.
 */

public class StockGraphActivity extends AppCompatActivity implements OnChartValueSelectedListener {
    private StockData mStock;
    private BarChart mChart;
    private TextView mMaxProfit;
//    private Typeface mTfLight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        Log.i("TAG", getAssets().toString());
//        mTfLight = Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);


        if(getIntent() != null && getIntent().getExtras() != null){
            mStock = (StockData) getIntent().getExtras().getParcelable(Constants.STOCK_DATA);
            actionBar.setTitle(mStock.getStockName()+ " ("+mStock.getStockTicker()+")");

            mMaxProfit = (TextView) findViewById(R.id.tv_max_profit);
            mChart = (BarChart) findViewById(R.id.chart);

            initializeChart();
            setGraphData(mStock.getStockValue());
            setAnalysisData();
        }

    }

    private void setAnalysisData() {
        ArrayList<Trade> trades = mStock.findBestTrades(2);
        mMaxProfit.setText("Max Profits on consecutive trades: \u20B9"+String.valueOf(mStock.findMaxProfit()+"\n"+
                "Best Trade: \u20B9"+trades.get(0).getBuyPrice()+"(Buy) -- \u20B9"+trades.get(0).getSellPrice()+"(Sell) \n" +
                "2nd Best Trade: \u20B9"+trades.get(1).getBuyPrice()+"(Buy) -- \u20B9"+trades.get(1).getSellPrice()+"(Sell)"));

    }

    private void initializeChart() {
        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);

        mChart.setMaxVisibleValueCount(60);

        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);

        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(mStock.getStockValue().length/2 +1);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new PercentFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(100f);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f);
        rightAxis.setAxisMaximum(100f);

//        Legend l = mChart.getLegend();
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//        l.setForm(Legend.LegendForm.SQUARE);
//        l.setFormSize(9f);
//        l.setTextSize(11f);
//        l.setXEntrySpace(4f);

        XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
        mv.setChartView(mChart);
        mChart.setMarker(mv);
    }

    private void setGraphData(int[] prices) {

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        for (int i = 0; i < prices.length; i++) {
            yVals1.add(new BarEntry(i, ((float)prices[i])/10f));
        }

        BarDataSet set1;
        int[] finalColors = getBarColors();

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);

            set1.setColors(finalColors);

            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
            mChart.invalidate();
        } else {
            set1 = new BarDataSet(yVals1, "Stock prices of "+ mStock.getStockName());
            set1.setDrawIcons(false);
            set1.setColors(finalColors);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);

            mChart.setData(data);
        }
    }

    private int[] getBarColors() {

        int[] finalColors = new int[mStock.getStockValue().length];
        ArrayList<Trade> trades = mStock.findBestTrades(2);
        ArrayList<Integer> exemptedArray = new ArrayList<>();
        int bestBuyIndex = Helper.getIndex( mStock.getStockValue(), trades.get(0).getBuyPrice(), true, exemptedArray);
        exemptedArray.add(bestBuyIndex);
        int secondBuyIndex = Helper.getIndex( mStock.getStockValue(), trades.get(1).getBuyPrice(), true, exemptedArray);
        exemptedArray.add(secondBuyIndex);
        int bestSellIndex = Helper.getIndex( mStock.getStockValue(), trades.get(0).getSellPrice(), false, exemptedArray);
        exemptedArray.add(bestSellIndex);
        int secondSellIndex = Helper.getIndex( mStock.getStockValue(), trades.get(1).getSellPrice(), false, exemptedArray);

        for (int i = 0; i < finalColors.length ; i++) {
            if(i == bestBuyIndex){
                finalColors[i] = ContextCompat.getColor(this,android.R.color.holo_green_dark);
            } else if( i == secondBuyIndex){
                finalColors[i] = ContextCompat.getColor(this,android.R.color.holo_green_light);
            } else if( i == bestSellIndex){
                finalColors[i] = ContextCompat.getColor(this,android.R.color.holo_green_dark);
            } else if( i == secondSellIndex){
                finalColors[i] = ContextCompat.getColor(this,android.R.color.holo_green_light);
            } else {
                finalColors[i] = ContextCompat.getColor(this,android.R.color.darker_gray);
            }
        }
        return finalColors;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }

            case R.id.updateData: {
                mStock.updateStockPrice();
                setGraphData(mStock.getStockValue());
                setAnalysisData();
                break;
            }
        }
        return true;
    }
}
